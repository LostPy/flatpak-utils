use self::{
    app::Application,
    option::{InstallOptions, Installation, Level},
};
use indicatif::ProgressBar;
use std::{
    fmt::Debug,
    fs::File,
    io::{Read, Write},
    str,
};
use std::{path::Path, process::Command};

mod app;
mod option;

/// Install application from a yaml file
pub fn install_from_file<P: AsRef<Path> + Debug>(path: P) -> std::io::Result<()> {
    log::trace!("Get data from the yaml file ({:?})...", &path);
    let mut file = File::open(path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    log::trace!("parse applications");
    let apps: Vec<Application> = serde_yaml::from_str(&contents).unwrap();
    let app_count = apps.len();
    let progress = ProgressBar::new(app_count as u64);
    log::info!("Installing applications...");
    for app in apps.iter() {
        let options = &app.options;
        match options.installation.clone() {
            Installation::Default => Command::new("flatpak")
                .arg("install")
                .arg("--assumeyes")
                .arg(options.level.arg())
                .arg(options.repository.arg())
                .arg(app.id.clone())
                .output()
                .expect("Failed to execute flatpak install command"),
            Installation::Custom { name: _ } => Command::new("flatpak")
                .arg("install")
                .arg("--assumeyes")
                .arg(options.installation.arg())
                .arg(options.repository.arg())
                .arg(app.id.clone())
                .output()
                .expect("Failed to execute flatpak install command"),
        };
        progress.inc(1);
    }
    progress.finish();
    log::info!("Applications installed with success");
    Ok(())
}

/// Save installed applications in a yaml file
pub fn save_applications<P: AsRef<Path> + Debug>(path: P) -> std::io::Result<()> {
    log::debug!("Exporting application in a file...");
    let output_cmd = Command::new("flatpak")
        .arg("list")
        .arg("--app")
        .arg("--columns=name,application,installation")
        .output()
        .expect("Failed to execute flatpak list command");
    if output_cmd.status.success() {
        log::info!("Parse the list of applications...");
        let apps = parse_list_application(
            str::from_utf8(output_cmd.stdout.as_slice())
                .unwrap()
                .to_string(),
        );
        log::trace!("Create the yaml file...");
        let mut file = File::create(path)?;
        log::trace!("Write the list of application in the yaml file...");
        file.write_all(serde_yaml::to_string(&apps).unwrap().as_bytes())?;
        log::info!("Applications list saved");
    } else {
        log::error!(
            "Command `flatpak list` exit with error: {}",
            str::from_utf8(output_cmd.stdout.as_slice()).unwrap()
        );
    }
    Ok(())
}

fn parse_list_application(text: String) -> Vec<Application> {
    log::debug!("Parsing the list of application...");
    let mut apps = Vec::new();
    for line in text.split('\n') {
        let mut fields = line.split('\t');
        log::trace!("Application raw data: {:?}", &fields);
        let app_name = fields.next().unwrap().trim().to_string();
        if app_name.is_empty() {
            break;
        }
        let app_id = fields.next().unwrap().trim().to_string();
        let install_options = fields
            .next()
            .unwrap()
            .trim()
            .split(' ')
            .collect::<Vec<&str>>();
        let installation_level = Level::from(install_options[0]);
        let installation = match install_options.get(1) {
            Some(value) => Installation::from(value.trim_start_matches('(').trim_end_matches(')')),
            None => Installation::Default,
        };
        log::trace!("Add application to the list: {}", &app_name);
        apps.push(
            Application::new(app_name, app_id)
                .options(InstallOptions::new(installation, installation_level)),
        )
    }
    apps
}

pub trait FlatpakArgument {
    fn arg(&self) -> String;
}
