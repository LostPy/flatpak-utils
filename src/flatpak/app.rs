use super::option::InstallOptions;
use serde::{Deserialize, Serialize};

/// Define a Flatpak application
#[derive(Debug, Serialize, Deserialize)]
pub struct Application {
    /// Name of the application (For example 'Firefox')
    pub name: String,
    /// Flatpak ID of the application (For example `org.mozilla.firefox`)
    pub id: String,
    /// Options of installation
    pub options: InstallOptions,
}

impl Application {
    pub fn new(name: String, id: String) -> Self {
        Self {
            name,
            id,
            options: InstallOptions::default(),
        }
    }

    pub fn options(mut self, options: InstallOptions) -> Self {
        self.options = options;
        self
    }
}
