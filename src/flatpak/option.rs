use serde::{Deserialize, Serialize};

use super::FlatpakArgument;

/// Options of installation for a Flatpak application
#[derive(Debug, Default, Serialize, Deserialize)]
#[serde(default)]
pub struct InstallOptions {
    pub installation: Installation,
    pub level: Level,
    pub repository: RemoteRepository,
}

impl InstallOptions {
    pub fn new(installation: Installation, level: Level) -> Self {
        Self {
            installation,
            level,
            ..Default::default()
        }
    }
}

/// Enum the differents installation mode
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum Installation {
    Default,
    Custom { name: String },
}

impl Default for Installation {
    fn default() -> Self {
        Self::Default
    }
}

impl From<&str> for Installation {
    fn from(value: &str) -> Self {
        match value.to_lowercase().as_str() {
            "default" => Self::Default,
            _ => Self::Custom {
                name: value.to_string(),
            },
        }
    }
}

impl From<&Installation> for String {
    fn from(value: &Installation) -> Self {
        match value {
            Installation::Default => "default".to_string(),
            Installation::Custom { name } => name.clone(),
        }
    }
}

impl FlatpakArgument for Installation {
    fn arg(&self) -> String {
        format!("--installation={name}", name = String::from(self))
    }
}

/// Level of installation (system or user)
#[derive(Debug, Serialize, Deserialize)]
pub enum Level {
    Default,
    System,
    User,
}

impl Default for Level {
    fn default() -> Self {
        Self::Default
    }
}

impl From<&str> for Level {
    fn from(value: &str) -> Self {
        match value.to_lowercase().as_str() {
            "default" => Self::Default,
            "system" => Self::System,
            "user" => Self::User,
            _ => panic!("level doesn't exists ({value})"),
        }
    }
}

impl FlatpakArgument for Level {
    fn arg(&self) -> String {
        match self {
            Level::Default | Level::System => "--system".to_string(),
            Level::User => "--user".to_string(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum RemoteRepository {
    Flathub,
    Other { name: String },
}

impl Default for RemoteRepository {
    fn default() -> Self {
        Self::Flathub
    }
}

impl FlatpakArgument for RemoteRepository {
    fn arg(&self) -> String {
        match self {
            Self::Flathub => "flathub".to_string(),
            Self::Other { name } => name.clone(),
        }
    }
}
