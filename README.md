# Flatpak Utils

A small CLI to export installed applications from flatpak into a YAML file and install flatpak application from this file.

## Usage

### commands

```
Utils command to Flatpak.

            Save the list of installed applications and 
            install applications from this list.

Usage: flatpak-utils [COMMAND]

Commands:
  install  
  export   
  help     Print this message or the help of the given subcommand(s)

Options:
  -h, --help     Print help
  -V, --version  Print version
```

### export applications

```
flatpak-utils export flatpak-apps.yml
```

### install applications

```
flatpak-utils install flatpak-apps.yml
```
